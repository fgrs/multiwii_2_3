My quadcopter setup
===================

## Controller ##

MWC by AlexinParis design by Warthox www.flyduino.com

~~~
Gyroscope: Wii Motion Plus (WMP)
Accelerometer: BMA180
Barometer: MS5611 (MS561101BA)
Magnetometer: HMC5883
~~~

## Frame ##

Tarot 650 Ironman

~~~
ESC: ProFlight UK 30A SimonK
Motors: SunnySky V2216-12 800KV
~~~